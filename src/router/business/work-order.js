import WorkOrderTypeList from '@/components/business/work-order/work-order-type-list.vue'
import WorkOrderList from '@/components/business/work-order/work-order-list.vue'
import WorkOrderUserList from '@/components/business/work-order/work-order-user-list.vue'

export default [
  {
    path: '/WorkOrderTypeList',
    component: WorkOrderTypeList
  },
  {
    path: '/WorkOrderList',
    component: WorkOrderList
  },
  {
    path: '/WorkOrderUserList',
    component: WorkOrderUserList
  }
]
