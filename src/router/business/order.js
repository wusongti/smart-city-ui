import GoodsList from '@/components//business//order/goods/goods-list.vue'
import OrderList from '@/components//business//order/order/order-list.vue'
export default [{
		path: '/GoodsList',
		component: GoodsList
	},
	{
		path: '/OrderList',
		component: OrderList
	}
]
