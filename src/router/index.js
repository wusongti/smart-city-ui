import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/index/index.vue'
import TopIndex from '@/components/top-index.vue'
import Login from '@/components/login/login.vue'
import ResponseError from '@/components/response/response-error.vue'
import Response204 from '@/components/response/response-204.vue'
import Response401 from '@/components/response/response-401.vue'
import Response404 from '@/components/response/response-404.vue'
import AdminRouter from './common/admin'
import ResponseRouter from './common/response'
import DashboardRouter from './common/dashboard'
import Demo1Router from './business/demo1'
import Demo2Router from './business/demo2'
import WorkOrderRouter from './business/work-order'
import OrderRouter from './business/order'

Vue.use(Router)

let routes = []

export default new Router({
  routes: [{
    path: '/',
    component: Login
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path: '/ResponseError',
    name: 'ResponseError',
    component: ResponseError
  },
  {
    path: '/Response204',
    name: 'Response204',
    component: Response204
  },
  {
    path: '/Response401',
    name: 'Response401',
    component: Response401
  },
  {
    path: '/Response404',
    name: 'Response404',
    component: Response404
  },
  {
    path: '/Index',
    component: Index,
    children: routes.concat(AdminRouter, Demo1Router, Demo2Router,WorkOrderRouter, ResponseRouter, DashboardRouter, OrderRouter)
  },
  {
    path: '/TopIndex',
    component: TopIndex
  }
  ]
})
