import DashboardA100401 from '@/components/dashboard/dashboard-A100401.vue'
import DashboardA100403 from '@/components/dashboard/dashboard-A100403.vue'
import DashboardA100406 from '@/components/dashboard/dashboard-A100406.vue'
import DashboardA100409 from '@/components/dashboard/dashboard-A100409.vue'
import DashboardA100410 from '@/components/dashboard/dashboard-A100410.vue'
import DashboardA100411 from '@/components/dashboard/dashboard-A100411.vue'

export default [
  {
    path: '/DashboardA100401',
    component: DashboardA100401,
    meta: {
      title: '系统首页'
    }
  },
  {
    path: '/DashboardA100403',
    component: DashboardA100403,
    meta: {
      title: '系统首页'
    }
  },
  {
    path: '/DashboardA100406',
    component: DashboardA100406,
    meta: {
      title: '系统首页'
    }
  },
  {
    path: '/DashboardA100409',
    component: DashboardA100409,
    meta: {
      title: '系统首页'
    }
  },
  {
    path: '/DashboardA100410',
    component: DashboardA100410,
    meta: {
      title: '系统首页'
    }
  },
  {
    path: '/DashboardA100411',
    component: DashboardA100411,
    meta: {
      title: '系统首页'
    }
  }
]
