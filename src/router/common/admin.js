import ShortcutMenuList from '@/components/admin/shortcut-menu/shortcut-menu-list.vue'
import CompanyList from '@/components/admin/company/company-list.vue'
import DepartmentList from '@/components/admin/department/department-list.vue'
import ProjectList from '@/components/admin/project/project-list.vue'
import RoleList from '@/components/admin/role/role-list.vue'
import AccountList from '@/components/admin/account/account-list.vue'
import CustomerList from '@/components/admin/customer/customer-list.vue'
import UserList from '@/components/admin/user/user-list.vue'
import OrganizationIndex from '@/components/admin/organization/organization-index.vue'
import LookupIndex from '@/components/admin/data-dictionary/lookup-index.vue'
import MyImportExportList from '@/components/admin/my-import-export/my-import-export-list.vue'
import OperationLogList from '@/components/admin/operation-log/operation-log-list.vue'
import JobList from '@/components/admin/job/job-list.vue'
import AppTokenList from '@/components/admin/app-token/app-token-list.vue'
import NotificationList from '@/components/admin/notification/notification-list.vue'
import DataSourceList from '@/components/admin/data-source/data-source-list.vue'
import MyNoticeList from '@/components/admin/my-notice/my-notice-list.vue'
import PersonalCenter from '@/components/admin/personal-center/personal-center.vue'
import ServerMonitor from '@/components/admin/moitor/server-monitor.vue'
import Zipkin from '@/components/admin/moitor/zipkin.vue'
import Kibana from '@/components/admin/moitor/kibana.vue'
import Swagger from '@/components/admin/moitor/swagger.vue'
import TenantList from '@//components//admin/tenant/tenant-list.vue'
import PostList from '@//components//admin/post/post-list.vue'

export default [{
    path: '/ShortcutMenuList',
    component: ShortcutMenuList
  },
  {
    path: '/CompanyList',
    component: CompanyList
  },
  {
    path: '/DepartmentList',
    component: DepartmentList
  },
  {
    path: '/ProjectList',
    component: ProjectList
  },
  {
    path: '/RoleList',
    component: RoleList
  },
  {
    path: '/AccountList',
    component: AccountList
  },
  {
    path: '/CustomerList',
    component: CustomerList
  },
  {
    path: '/UserList',
    component: UserList
  },
  {
    path: '/OrganizationIndex',
    component: OrganizationIndex
  },
  {
    path: '/DataSourceList',
    component: DataSourceList
  },
  {
    path: '/LookupIndex',
    component: LookupIndex
  },
  {
    path: '/MyImportExportList',
    component: MyImportExportList
  },
  {
    path: '/OperationLogList',
    component: OperationLogList
  },
  {
    path: '/JobList',
    component: JobList
  },
  {
    path: '/AppTokenList',
    component: AppTokenList
  },
  {
    path: '/NotificationList',
    component: NotificationList
  },
  {
    path: '/MyNoticeList',
    component: MyNoticeList,
    meta: {
      title: '我的通知列表'
    }
  },
  {
    path: '/PersonalCenter',
    component: PersonalCenter,
    meta: {
      title: '个人中心'
    }
  },
  {
    path: '/ShortcutMenuList',
    component: ShortcutMenuList,
    meta: {
      title: '快捷入口'
    }
  },
  {
    path: '/ServerMonitor',
    component: ServerMonitor,
    meta: {
      title: '服务监控'
    }
  },
  {
    path: '/Zipkin',
    component: Zipkin,
    meta: {
      title: '链路追踪'
    }
  },
  {
    path: '/Kibana',
    component: Kibana,
    meta: {
      title: '日志追踪'
    }
  },
  {
    path: '/Swagger',
    component: Swagger,
    meta: {
      title: '文档聚合'
    }
  },
  {
    path: '/TenantList',
    component: TenantList
  },
  {
    path: '/PostList',
    component: PostList
  }
]
