// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './app'
import router from './router'
import VueI18n from 'vue-i18n'
import enUS from './common/i18n/lang/en-US'
import zhCN from './common/i18n/lang/zh-CN'
import './common/interceptor/http-interceptors'
import LoginContext from './common/login-context'
import UniversalPlugin from './common/plugin/universal-plugin'
import DirectivePlugin from './common/plugin/directive-plugin'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '../element-variables.scss'

import AMap from 'vue-amap'

/* ********************富文本 S *************************/
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
/* ********************富文本 E *************************/

Vue.use(AMap)

Vue.use(VueQuillEditor)

Vue.config.productionTip = false

Vue.prototype.loginContext = LoginContext

Vue.use(VueI18n)

Vue.use(UniversalPlugin)

Vue.use(DirectivePlugin)

Vue.use(ElementUI)

Vue.filter('formatDate', function (date, frm) {
  return Vue.$formatDate(date, frm)
})

const i18n = new VueI18n({
  locale: localStorage.getItem('locale') || 'zh-CN',
  messages: {
    'zh-CN': zhCN,
    'en-US': enUS
  }
})

// 初始化vue-amap
AMap.initAMapApiLoader({
  // 高德key
  key: 'b4a59a458cbf336523e4077ccee16600',
  // 插件集合 （插件按需引入）
  plugin: ['AMap.Geolocation']
})

// 监听sessionStorage和localStorage的setItem事件
Vue.prototype.$addStorageEvent = function (type, key, data) {
  if (type === 1) {
    // 创建一个StorageEvent事件
    let newStorageEvent = document.createEvent('StorageEvent')
    const storage = {
      setItem: function (k, val) {
        localStorage.setItem(k, val)
        // 初始化创建的事件
        newStorageEvent.initStorageEvent('setItem', false, false, k, null, val, null, null)
        // 派发对象
        window.dispatchEvent(newStorageEvent)
      }
    }
    return storage.setItem(key, data)
  } else {
    // 创建一个StorageEvent事件
    let newStorageEvent = document.createEvent('StorageEvent')
    const storage = {
      setItem: function (k, val) {
        sessionStorage.setItem(k, val)
        // 初始化创建的事件
        newStorageEvent.initStorageEvent('setItem', false, false, k, null, val, null, null)
        // 派发对象
        window.dispatchEvent(newStorageEvent)
      }
    }
    return storage.setItem(key, data)
  }
}

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  i18n,
  router,
  components: { App },
  template: '<App/>'
})
