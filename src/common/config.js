/**
 * Created by wust on 2019/5/6.
 */

let config = require('../../config')
let isProduction = process.env.NODE_ENV === 'production'
let isTesting = process.env.NODE_ENV === 'testing'

export const BASE_URL = isProduction ? config.build.BASE_URL : (isTesting ? config.test.BASE_URL : config.dev.BASE_URL)
export const ADMIN_SERVER_URL = isProduction ? (config.build.BASE_URL + '/admin-server') : (isTesting ? config.test.BASE_URL + '/admin-server' : config.dev.BASE_URL + 'webapi/admin-server')
export const AUTOTASK_SERVER_URL = isProduction ? (config.build.BASE_URL + '/autotask-server') : (isTesting ? config.test.BASE_URL + '/autotask-server' : config.dev.BASE_URL + 'webapi/autotask-server')
export const BUSINESSLOG_SERVER_URL = isProduction ? (config.build.BASE_URL + '/businesslog-server') : (isTesting ? config.test.BASE_URL + '/businesslog-server' : config.dev.BASE_URL + 'webapi/businesslog-server')
export const DEMO1_SERVER_URL = isProduction ? (config.build.BASE_URL + '/demo1-server') : (isTesting ? config.test.BASE_URL + '/demo1-server' : config.dev.BASE_URL + 'webapi/demo1-server')
export const DEMO2_SERVER_URL = isProduction ? (config.build.BASE_URL + '/demo2-server') : (isTesting ? config.test.BASE_URL + '/demo2-server' : config.dev.BASE_URL + 'webapi/demo2-server')
export const WORK_ORDER_SERVER_URL = isProduction ? (config.build.BASE_URL + '/work-order-server') : (isTesting ? config.test.BASE_URL + '/work-order-server' : config.dev.BASE_URL + 'webapi/work-order-server')
export const ORDER_SERVER_URL = isProduction ? (config.build.BASE_URL + '/order-server') : (isTesting ? config.test.BASE_URL + '/order-server' : config.dev.BASE_URL + 'webapi/order-server')
export const RABBITMQ_URL = isProduction ? (config.build.RABBITMQ_URL) : (isTesting ? config.test.RABBITMQ_URL : config.dev.RABBITMQ_URL)
export const RABBITMQ_USERNAME = isProduction ? (config.build.RABBITMQ_USERNAME) : (isTesting ? config.test.RABBITMQ_USERNAME : config.dev.RABBITMQ_USERNAME)
export const RABBITMQ_PASSWORD = isProduction ? (config.build.RABBITMQ_PASSWORD) : (isTesting ? config.test.RABBITMQ_PASSWORD : config.dev.RABBITMQ_PASSWORD)

export const PROFILES_ACTIVE = isProduction ? 'prod' : (isTesting ? 'test' : 'dev')
