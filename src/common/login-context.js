/**
 * 登录上下文，可通过该类设置和获取当前登录用户的上下文信息，上下文信息会存储在session里面
 * @author wust
 * @type {{loginContextModel: {loginName: string, oneLevelMenus: Array, permissions: Array, xAuthToken: string}, setLoginContext: loginContext.setLoginContext, getLoginContext: loginContext.getLoginContext}}
 */
let loginContext = {
  setLoginContext: function (p) {
    let obj = p.data.obj

    let loginContextModel = {
      accountId: obj.accountId,
      loginName: obj.loginName,
      realName: obj.realName,
      userType: obj.userType,
      oneLevelMenus: obj.oneLevelMenus,
      menuNames: obj.menuNames,
      permissions: obj.permissions,
      projectId: null
    }

    sessionStorage.setItem('x-auth-token', obj.xAuthToken)
    sessionStorage.setItem('loginContextModel', JSON.stringify(loginContextModel))
    sessionStorage.setItem('my-project-list', JSON.stringify(obj.myProjectList))
  },
  getLoginContext: function () {
    let loginContextSession = sessionStorage.getItem('loginContextModel')
    if (loginContextSession === undefined || loginContextSession == null || loginContextSession === '') {
      this.$router.push({path: '/Login'})
    } else {
      return JSON.parse(loginContextSession)
    }
  },
  removeLoginContext: function () {
    sessionStorage.removeItem('x-auth-token')
    sessionStorage.removeItem('loginContextModel')
    sessionStorage.removeItem('lastVisitMenu')
  },
  setProjectToLoginContext: function (projectId) {
    let loginContextSession = sessionStorage.getItem('loginContextModel')
    if (loginContextSession === undefined || loginContextSession == null || loginContextSession === '') {
      this.$router.push({path: '/Login'})
    } else {
      let context = JSON.parse(loginContextSession)
      context.projectId = projectId
      sessionStorage.removeItem('loginContextModel')
      sessionStorage.setItem('loginContextModel', JSON.stringify(context))
    }
  }
}

export default loginContext
