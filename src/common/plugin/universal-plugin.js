import axios from 'axios'
import MessageComponent from '../component/message-component.vue'
import DialogComponent from '../component/dialog-component.vue'
import crypto from 'crypto'

const Config = require('../config.js')
/**
 * Created by wust on 2019/5/15.
 */
let universalPlugin = {
	install: function(Vue, options) {
		// 全局属性：baseUrl
		Vue.$baseURL = Config.BASE_URL

		// 全局属性：ADMIN_SERVER_URL
		Vue.$adminServerURL = Config.ADMIN_SERVER_URL

		// 全局属性：AUTOTASK_SERVER_URL
		Vue.$autotaskServerURL = Config.AUTOTASK_SERVER_URL

		// 全局属性：BUSINESSLOG_SERVER_URL
		Vue.$businesslogServerURL = Config.BUSINESSLOG_SERVER_URL

		// 全局属性：DEMO1_SERVER_URL
		Vue.$demo1ServerURL = Config.DEMO1_SERVER_URL

		// 全局属性：DEMO2_SERVER_URL
		Vue.$demo2ServerURL = Config.DEMO2_SERVER_URL

		// 全局属性：WORK_ORDER_SERVER_URL
		Vue.$workOrderServerURL = Config.WORK_ORDER_SERVER_URL

		// 全局属性：ORDER_SERVER_URL
		Vue.$orderServerURL = Config.ORDER_SERVER_URL


		// 全局属性：RABBITMQ_URL
		Vue.$rabbitmqURL = Config.RABBITMQ_URL

		// 全局属性：RABBITMQ_USERNAME
		Vue.$rabbitmqUserName = Config.RABBITMQ_USERNAME

		// 全局属性：RABBITMQ_PASSWORD
		Vue.$rabbitmqPassword = Config.RABBITMQ_PASSWORD

		// 全局属性：PROFILES_ACTIVE
		Vue.$profilesActive = Config.PROFILES_ACTIVE

		// 全局属性：ajax
		Vue.$ajax = axios

		// 全局属性：开放接口调用appId
		Vue.$appId = '0561E290842A11EA95FB84FDD1782A43'

		// 全局属性：开放接口调用secretKey
		Vue.$secretKey = '0DF918F1842A11EA95FB84FDD1782A43'

		// 实例方法：返回
		Vue.prototype.$goBack = function() {
			window.history.length > 1 ? this.$router.go(-1) : this.$router.push('/')
		}

		Vue.$unique = function(array) {
			let res = array.filter(function(item, index, array) {
				return array.indexOf(item) === index
			})
			return res
		}


		// 全局静态方法：开放接口签名
		Vue.$sign = function(params) {
			let paramsNew = 'appId=' + Vue.$appId + '&' + params //
			let paramsNewArray = paramsNew.split('&')
			let paramsSortArray = paramsNewArray.sort()
			let link = '';
			for (let i = 0; i < paramsSortArray.length; i++) {
				if (link === '') {
					link += paramsSortArray[i]
				} else {
					link += '&' + paramsSortArray[i]
				}
			}
			let sign = Vue.$md5(link + '&secretKey=' + Vue.$secretKey).toUpperCase()
			return sign
		}



		// 全局静态方法：去空
		Vue.$trim = function(v) {
			return (v + '').replace(/(^\s+)|(\s+$)/g, '')
		}

		// 全局静态方法：空白判断
		Vue.$isBlank = function(v) {
			if (Vue.$trim(v) === '') {
				return true
			}
			return false
		}

		// 全局静态方法：null判断
		Vue.$isNull = function(v) {
			if (v == null) {
				return true
			}

			if (Vue.$trim(v).toLowerCase() === 'null') {
				return true
			}
			return false
		}

		// 全局静态方法：空白判断
		Vue.$isUndefined = function(v) {
			if (v === undefined) {
				return true
			}

			if (Vue.$trim(v).toLowerCase() === 'undefined') {
				return true
			}
			return false
		}

		// 全局静态方法：isNull,isBlank,isUndefined组合
		Vue.$isNullOrIsBlankOrIsUndefined = function(v) {
			if (Vue.$isNull(v)) {
				return true
			}

			if (Vue.$isBlank(v)) {
				return true
			}

			if (Vue.$isUndefined(v)) {
				return true
			}
			return false
		}

		// 全局静态方法：密码格式判断
		Vue.$isPassword = function(v) {
			let reg = /^[0-9a-zA-Z_]{1,}$/
			if (reg.test(v)) {
				return v
			}
		}

		// 全局静态方法：日期格式判断
		Vue.$isDate = function(v) {
			// eslint-disable-next-line no-useless-escape
			let reg = /^((\d{4})[\-|\.|\/](\d{1,2})[\-|\.|\/](\d{1,2}))?$/
			if (reg.test(v)) {
				return v
			}
		}

		// 全局静态方法：日期格式判断
		Vue.$isEmail = function(v) {
			let reg = /^(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)?$/
			if (reg.test(v)) {
				return v
			}
		}

		// 全局静态方法：数字判断
		Vue.$isNumber = function(v) {
			// eslint-disable-next-line no-useless-escape
			let reg = /(^[\-0-9][0-9]*(.[0-9]+)?)$/
			if (reg.test(v)) {
				return v
			}
		}

		// 全局静态方法：移动电话判断
		Vue.$isMobile = function(v) {
			let reg = /^(0?1[358]\d{9})*$/
			if (reg.test(v)) {
				return v
			}
		}

		// 全局静态方法：格式化日期
		Vue.$formatDate = function(date, fmt) {
			if (Vue.$isNullOrIsBlankOrIsUndefined(date)) {
				return null
			}

			let v = date
			if (!(v instanceof Date)) {
				v = new Date(date)
			}

			if (/(y+)/.test(fmt)) {
				fmt = fmt.replace(RegExp.$1, (v.getFullYear() + '').substr(4 - RegExp.$1.length))
			}
			let o = {
				'M+': v.getMonth() + 1,
				'd+': v.getDate(),
				'h+': v.getHours(),
				'm+': v.getMinutes(),
				's+': v.getSeconds()
			}
			for (let k in o) {
				if (new RegExp(`(${k})`).test(fmt)) {
					let str = o[k] + ''
					fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : ('00' + str).substr(str.length))
				}
			}
			return fmt
		}

		// 全局静态方法：md5加密
		Vue.$md5 = function(v) {
			let md5 = crypto.createHash('md5')
			md5.update(v)
			return md5.digest('hex')
		}

		// 全局静态方法：判断数组是否存在指定值
		Vue.$hasValInArrayObj = function(arr, key, val) {
			for (let i = 0; i < arr.length; i++) {
				if (arr[i][key] === val) {
					return i
				}
			}
			return -1
		}

		/** ********************** 警告框 S ****************************/
		// 生成一个Vue的子类
		let MessageComponentConstructor = Vue.extend(MessageComponent)
		let messageComponentConstructorInstance = new MessageComponentConstructor()

		// 将这个实例挂载在div上
		messageComponentConstructorInstance.$mount(document.createElement('div'))

		// 并将此div加入最顶层的body里面
		document.body.appendChild(messageComponentConstructorInstance.$el)

		Vue.prototype.$message = (type, msg, duration = 5000) => {
			messageComponentConstructorInstance.message = msg
			messageComponentConstructorInstance.messageType = type
			setTimeout(() => {
				messageComponentConstructorInstance.messageType = ''
			}, duration)
		}
		/** ********************** 警告框 E ****************************/

		/** ********************** 对话框 S ****************************/
		// 生成一个Vue的子类
		let DialogComponentConstructor = Vue.extend(DialogComponent)
		let dialogComponentConstructorInstance = new DialogComponentConstructor()

		// 将这个实例挂载在div上
		dialogComponentConstructorInstance.$mount(document.createElement('div'))

		// 并将此div加入最顶层的body里面
		document.body.appendChild(dialogComponentConstructorInstance.$el)

		Vue.prototype.$dialog = (title, message, showOKButton, showCancelButton, fnOK, fnCancel) => {
			dialogComponentConstructorInstance.title = title
			dialogComponentConstructorInstance.message = message
			dialogComponentConstructorInstance.showOKButton = showOKButton
			dialogComponentConstructorInstance.showCancelButton = showCancelButton
			dialogComponentConstructorInstance.showDialog = true
			dialogComponentConstructorInstance.fnOK = fnOK
			dialogComponentConstructorInstance.fnCancel = fnCancel
		}
		/** ********************** 对话框 E ****************************/
	}
}
export default universalPlugin
