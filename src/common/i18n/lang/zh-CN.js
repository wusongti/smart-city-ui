// eslint-disable-next-line camelcase
const zhCN = {
  // 规则：目录名.页面名.标签
  components: {
    common: {
      systemShortName: '智慧社区SAAS服务平台',
      systemName: '智慧社区SAAS服务平台',
      companyShortName: 'XXX科技有限公司',
      companyName: 'XXX科技有限公司',
      address: '全国服务热线：XXX-XXX-XXX',
      rightDescription: 'XXX Copyright © 2020 All rights reserved'
    },
    login: {
      login: {
        label1: '登录账号',
        label2: '登录口令',
        label3: '语言',
        label4: '登&nbsp;&nbsp;录',
        label5: '账&nbsp;&nbsp;&nbsp;&nbsp;号&nbsp;&nbsp;&nbsp;&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录',
        label6: '扫&nbsp;&nbsp;&nbsp;&nbsp;码&nbsp;&nbsp;&nbsp;&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录',
        label7: '欢&nbsp;迎&nbsp;使&nbsp;用&nbsp;智&nbsp;慧&nbsp;社&nbsp;区&nbsp服&nbsp;务&nbsp;平&nbsp;台',
        label8: '获取验证码',
        label9: '密码登录',
        label10: '验证码登录',
        label11: '扫码登录',

        msg1: '请输入登录账号',
        msg2: '请输入登录密码',
        msg3: '请输入验证码'
      }
    }
  }
}
// eslint-disable-next-line camelcase
export default zhCN
