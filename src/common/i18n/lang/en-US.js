// eslint-disable-next-line camelcase
const enUS = {
  // 规则：目录名.页面名.标签
  components: {
    common: {
      systemShortName: '智慧社区SAAS服务平台',
      systemName: '智慧社区SAAS服务平台',
      companyShortName: 'XXX科技有限公司',
      companyName: 'XXX科技有限公司',
      address: '全国服务热线：XXX-XXX-XXX',
      rightDescription: 'XXX Copyright © 2020 All rights reserved'
    },
    login: {
      login: {
        label1: 'Account',
        label2: 'Password',
        label3: 'Language',
        label4: 'Sign in',
        label5: 'Account login',
        label6: 'QR-Code login',
        label7: '欢&nbsp;迎&nbsp;使&nbsp;用&nbsp;智&nbsp;慧&nbsp;社&nbsp;区&nbsp服&nbsp;务&nbsp;平&nbsp;台',
        label8: '获取验证码',
        label9: '密码登录',
        label10: '验证码登录',
        label11: '扫码登录',

        msg1: 'The login name is required',
        msg2: 'The password is required',
        msg3: 'The verification-code is required'
      }
    }
  }
}
// eslint-disable-next-line camelcase
export default enUS
