module.exports = {
  proxy: {
    '/webapi': {
      target: 'http://localhost:8773',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/webapi': ''
      }
    },
    '/monitor-server': {
      target: 'http://localhost:8782/monitor-server',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/monitor-server': ''
      }
    },
    '/zipkin': {
      target: 'http://localhost:9411/zipkin',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/zipkin': ''
      }
    },
    '/kibana': {
      target: 'http://localhost:5601',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/kibana': ''
      }
    },
    '/swagger-ui.html': {
      target: 'http://localhost:8773/swagger-ui.html',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/swagger-ui.html': ''
      }
    },
    '/swagger-resources': {
      target: 'http://localhost:8773/swagger-resources',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/swagger-resources': ''
      }
    },
    '/v2/api-docs': {
      target: 'http://localhost:8773/v2/api-docs',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/v2/api-docs': ''
      }
    },
    '/webjars': {
      target: 'http://localhost:8773/webjars',
      secure: false, // 如果是https接口，需要配置这个参数
      changeOrigin: true, // 是否跨域
      pathRewrite: {
        '^/webjars': ''
      }
    }
  }
}
